package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.PersonaContacto;

public interface PersonaContactoRepository extends JpaRepository<PersonaContacto, Integer>{

}
