package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.CuartaLocalidad;

public interface CuartaLocalidadRepository extends JpaRepository<CuartaLocalidad, Integer>{

}
