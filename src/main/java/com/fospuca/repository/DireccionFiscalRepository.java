package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.DireccionFiscal;

public interface DireccionFiscalRepository extends JpaRepository<DireccionFiscal, Integer>{

}
