package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.TerceraLocalidad;

public interface TerceraLocalidadRepository extends JpaRepository<TerceraLocalidad, Integer>{

}
