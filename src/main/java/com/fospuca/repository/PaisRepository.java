package com.fospuca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.Pais;

public interface PaisRepository extends JpaRepository<Pais, Integer>{
	//List<Pais> findByName(String nombre);
}
