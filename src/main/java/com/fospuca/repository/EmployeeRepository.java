package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
