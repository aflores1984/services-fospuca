package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.ActividadSeniat;

public interface ActividadSeniatRepository extends JpaRepository<ActividadSeniat, Integer>{

}
