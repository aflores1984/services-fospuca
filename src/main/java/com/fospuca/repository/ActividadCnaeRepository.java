package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.ActividadCnae;

public interface ActividadCnaeRepository extends JpaRepository<ActividadCnae, Integer>{

}
