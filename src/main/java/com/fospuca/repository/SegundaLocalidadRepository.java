package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.SegundaLocalidad;

public interface SegundaLocalidadRepository extends JpaRepository<SegundaLocalidad, Integer>{

}
