package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.Ciudad;

public interface CiudadRepository extends JpaRepository<Ciudad, Integer>{

}
