package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.Municipio;

public interface MunicipioRepository extends JpaRepository<Municipio, Integer>{

}
