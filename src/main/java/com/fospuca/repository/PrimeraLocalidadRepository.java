package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.PrimeraLocalidad;

public interface PrimeraLocalidadRepository extends JpaRepository<PrimeraLocalidad, Integer> {

}
