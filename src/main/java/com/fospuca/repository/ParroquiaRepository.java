package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.Parroquia;

public interface ParroquiaRepository extends JpaRepository<Parroquia, Integer>{

}
