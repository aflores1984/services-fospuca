package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.TipoContribuyente;

public interface TipoContribuyenteRepository  extends JpaRepository<TipoContribuyente, Integer>{

}
