package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.DireccionServicio;

public interface DireccionServicioRepository extends JpaRepository<DireccionServicio, Integer>{

}
