package com.fospuca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fospuca.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Integer>{
		
}
