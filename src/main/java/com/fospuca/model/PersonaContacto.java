package com.fospuca.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "personacontacto")
@EntityListeners(AuditingEntityListener.class)
public class PersonaContacto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@NotBlank
	@Column(name = "nombre")
	private String nombre;
	
	private String cedula;
	
	private String telefonoContacto1;
	
	private String telefonoContacto2;
	
	private String telefonoContacto3;
	
	private String emailContacto1;
	
	private String emailContacto2;
	
	private String emailContacto3;
	
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date fechaCreacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getTelefonoContacto1() {
		return telefonoContacto1;
	}

	public void setTelefonoContacto1(String telefonoContacto1) {
		this.telefonoContacto1 = telefonoContacto1;
	}

	public String getTelefonoContacto2() {
		return telefonoContacto2;
	}

	public void setTelefonoContacto2(String telefonoContacto2) {
		this.telefonoContacto2 = telefonoContacto2;
	}

	public String getTelefonoContacto3() {
		return telefonoContacto3;
	}

	public void setTelefonoContacto3(String telefonoContacto3) {
		this.telefonoContacto3 = telefonoContacto3;
	}

	public String getEmailContacto1() {
		return emailContacto1;
	}

	public void setEmailContacto1(String emailContacto1) {
		this.emailContacto1 = emailContacto1;
	}

	public String getEmailContacto2() {
		return emailContacto2;
	}

	public void setEmailContacto2(String emailContacto2) {
		this.emailContacto2 = emailContacto2;
	}

	public String getEmailContacto3() {
		return emailContacto3;
	}

	public void setEmailContacto3(String emailContacto3) {
		this.emailContacto3 = emailContacto3;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
