package com.fospuca.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "cuartalocalidad")
@EntityListeners(AuditingEntityListener.class)
public class CuartaLocalidad {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@NotBlank
	@Column(name = "cuartalocalidad")
	private String cuartalocalidad;
	
	private String slug;
	
	@Column(name = "descripcionCuarta")
	private String descripcionCuarta;
	
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date fechaCreacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCuartalocalidad() {
		return cuartalocalidad;
	}

	public void setCuartalocalidad(String cuartalocalidad) {
		this.cuartalocalidad = cuartalocalidad;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDescripcionCuarta() {
		return descripcionCuarta;
	}

	public void setDescripcionCuarta(String descripcionCuarta) {
		this.descripcionCuarta = descripcionCuarta;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
}
