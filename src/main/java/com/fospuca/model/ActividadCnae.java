package com.fospuca.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "actividadcnae")
@EntityListeners(AuditingEntityListener.class)
public class ActividadCnae {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "codigoCnae")
	private Integer codigoCnae;
	
	@Column(name = "actividadCnae")
	private String actividadCnae;
	
	@Column(name = "descripcionCnae")
	private String descripcionCnae;
	
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date fechaCreacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigoCnae() {
		return codigoCnae;
	}

	public void setCodigoCnae(Integer codigoCnae) {
		this.codigoCnae = codigoCnae;
	}

	public String getActividadCnae() {
		return actividadCnae;
	}

	public void setActividadCnae(String actividadCnae) {
		this.actividadCnae = actividadCnae;
	}

	public String getDescripcionCnae() {
		return descripcionCnae;
	}

	public void setDescripcionCnae(String descripcionCnae) {
		this.descripcionCnae = descripcionCnae;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}
