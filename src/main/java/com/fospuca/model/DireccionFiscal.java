package com.fospuca.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "direccionfiscal")
@EntityListeners(AuditingEntityListener.class)
public class DireccionFiscal {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "identificacion")
	private String identificacion;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "apellido")
	private String apellido;
	
	private Long posicion1;
	
	private Long posicion2;
	
	private Long posicion3;
	
	private Long posicion4;
	
	private String localidadCompleta;
	
	@Column(name = "observacion")
	private String observacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date fechaCreacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date fechaActivacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Long getPosicion1() {
		return posicion1;
	}

	public void setPosicion1(Long posicion1) {
		this.posicion1 = posicion1;
	}

	public Long getPosicion2() {
		return posicion2;
	}

	public void setPosicion2(Long posicion2) {
		this.posicion2 = posicion2;
	}

	public Long getPosicion3() {
		return posicion3;
	}

	public void setPosicion3(Long posicion3) {
		this.posicion3 = posicion3;
	}

	public Long getPosicion4() {
		return posicion4;
	}

	public void setPosicion4(Long posicion4) {
		this.posicion4 = posicion4;
	}

	public String getLocalidadCompleta() {
		return localidadCompleta;
	}

	public void setLocalidadCompleta(String localidadCompleta) {
		this.localidadCompleta = localidadCompleta;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

}
