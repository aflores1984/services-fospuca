package com.fospuca.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "actividadseniat")
@EntityListeners(AuditingEntityListener.class)
public class ActividadSeniat {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@NotBlank
	@Column(name = "actividadSeniat")
	private String actividadSeniat;
	
	@Column(name = "slug")
	private String slug;
	
	@Column(name = "descripcionSeniat")
	private String descripcionSeniat;
	
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date fechaCreacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getActividadSeniat() {
		return actividadSeniat;
	}

	public void setActividadSeniat(String actividadSeniat) {
		this.actividadSeniat = actividadSeniat;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDescripcionSeniat() {
		return descripcionSeniat;
	}

	public void setDescripcionSeniat(String descripcionSeniat) {
		this.descripcionSeniat = descripcionSeniat;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
}
