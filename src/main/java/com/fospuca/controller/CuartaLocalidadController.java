package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.CuartaLocalidadDao;
import com.fospuca.model.CuartaLocalidad;


@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class CuartaLocalidadController {
	
	@Autowired
	CuartaLocalidadDao cuartaLocalidadDao;
	
	/* to save an CuartaLocalidad*/
	@PostMapping("/cuartalocalidad")
	public CuartaLocalidad createCuartaLocalidad(@Valid @RequestBody CuartaLocalidad cuartaLocalidad) {
		return cuartaLocalidadDao.save(cuartaLocalidad);
	}
	
	/*get all CuartaLocalidad*/
	@GetMapping("/cuartalocalidad")
	public List<CuartaLocalidad> getAllCuartaLocalidad(){
		return cuartaLocalidadDao.findAll();
	}
	
	/*get CuartaLocalidad by CuartaLocalidadId*/
	@GetMapping("/cuartalocalidad/{id}")
	public ResponseEntity<CuartaLocalidad> getCuartaLocalidadById(@PathVariable(value="id") Integer cuartaLocalidadId){
		
		CuartaLocalidad cuartaLocalidad=cuartaLocalidadDao.findOne(cuartaLocalidadId);
		
		if(cuartaLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(cuartaLocalidad);
	}
	

	/*update an CuartaLocalidad by CuartaLocalidadId*/
	@PutMapping("/cuartalocalidad/{id}")
	public ResponseEntity<CuartaLocalidad> updateCuartaLocalidad(@PathVariable(value="id") Integer cuartaLocalidadId,@Valid @RequestBody CuartaLocalidad cuartaLocalidadDetails){
		
		CuartaLocalidad cuartaLocalidad=cuartaLocalidadDao.findOne(cuartaLocalidadId);
		if(cuartaLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		
		cuartaLocalidad.setCuartalocalidad(cuartaLocalidadDetails.getCuartalocalidad());
		cuartaLocalidad.setDescripcionCuarta(cuartaLocalidadDetails.getDescripcionCuarta());
		cuartaLocalidad.setSlug(cuartaLocalidadDetails.getSlug());
		
		CuartaLocalidad updateCuartaLocalidad=cuartaLocalidadDao.save(cuartaLocalidad);
		return ResponseEntity.ok().body(updateCuartaLocalidad);
	
	}
	
	/*Delete an CuartaLocalidad*/
	@DeleteMapping("/cuartalocalidad/{id}")
	public ResponseEntity<CuartaLocalidad> deleteCuartaLocalidad(@PathVariable(value="id") Integer cuartaLocalidadId){
		
		CuartaLocalidad cuartaLocalidad=cuartaLocalidadDao.findOne(cuartaLocalidadId);
		if(cuartaLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		cuartaLocalidadDao.delete(cuartaLocalidad);
		
		return ResponseEntity.ok().build();
	}
	
}
