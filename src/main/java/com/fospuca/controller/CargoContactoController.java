package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fospuca.dao.CargoContactoDao;
import com.fospuca.model.CargoContacto;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class CargoContactoController {
	
	@Autowired
	CargoContactoDao cargoContactoDao;
	
	/* to save an CargoContacto*/
	@PostMapping("/cargocontacto")
	public CargoContacto createCargoContacto(@Valid @RequestBody CargoContacto cargoContacto) {
		return cargoContactoDao.save(cargoContacto);
	}
	
	/*get all CargoContacto*/
	@GetMapping("/cargocontacto")
	public List<CargoContacto> getAllCargoContacto(){
		return cargoContactoDao.findAll();
	}
	
	/*get CargoContacto by CargoContactoId*/
	@GetMapping("/cargocontacto/{id}")
	public ResponseEntity<CargoContacto> getCargoContactoById(@PathVariable(value="id") Integer cargoContactoId){
		
		CargoContacto cargoContacto=cargoContactoDao.findOne(cargoContactoId);
		
		if(cargoContacto==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(cargoContacto);
	}
	

	/*update an CargoContacto by CargoContactoId*/
	@PutMapping("/cargocontacto/{id}")
	public ResponseEntity<CargoContacto> updateCargoContacto(@PathVariable(value="id") Integer cargoContactoId,@Valid @RequestBody CargoContacto cargoContactoDetails){
		
		CargoContacto cargoContacto=cargoContactoDao.findOne(cargoContactoId);
		if(cargoContacto==null) {
			return ResponseEntity.notFound().build();
		}
		
		cargoContacto.setCargo(cargoContactoDetails.getCargo());
		cargoContacto.setDescripcionCargo(cargoContactoDetails.getDescripcionCargo());
		cargoContacto.setSlug(cargoContactoDetails.getSlug());
		
		CargoContacto updateCargoContacto=cargoContactoDao.save(cargoContacto);
		return ResponseEntity.ok().body(updateCargoContacto);
	
	}
	
	/*Delete an CargoContacto*/
	@DeleteMapping("/cargocontacto/{id}")
	public ResponseEntity<CargoContacto> deleteCargoContacto(@PathVariable(value="id") Integer cargoContactoId){
		
		CargoContacto cargoContacto=cargoContactoDao.findOne(cargoContactoId);
		if(cargoContacto==null) {
			return ResponseEntity.notFound().build();
		}
		cargoContactoDao.delete(cargoContacto);
		
		return ResponseEntity.ok().build();
	
	}
	
}
