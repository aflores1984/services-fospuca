package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.TerceraLocalidadDao;
import com.fospuca.model.TerceraLocalidad;



@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class TerceraLocalidadController {
	
	@Autowired
	TerceraLocalidadDao terceraLocalidadDao;
	
	/* to save an TerceraLocalidad*/
	@PostMapping("/terceralocalidad")
	public TerceraLocalidad createTerceraLocalidad(@Valid @RequestBody TerceraLocalidad terceraLocalidad) {
		return terceraLocalidadDao.save(terceraLocalidad);
	}
	
	/*get all TerceraLocalidad*/
	@GetMapping("/terceralocalidad")
	public List<TerceraLocalidad> getAllTerceraLocalidad(){
		return terceraLocalidadDao.findAll();
	}
	
	/*get TerceraLocalidad by TerceraLocalidadId*/
	@GetMapping("/terceralocalidad/{id}")
	public ResponseEntity<TerceraLocalidad> getTerceraLocalidadById(@PathVariable(value="id") Integer terceraLocalidadId){
		
		TerceraLocalidad terceraLocalidad=terceraLocalidadDao.findOne(terceraLocalidadId);
		
		if(terceraLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(terceraLocalidad);
	}
	

	/*update an TerceraLocalidad by TerceraLocalidadId*/
	@PutMapping("/terceralocalidad/{id}")
	public ResponseEntity<TerceraLocalidad> updateTerceraLocalidad(@PathVariable(value="id") Integer terceraLocalidadId,@Valid @RequestBody TerceraLocalidad terceraLocalidadDetails){
		
		TerceraLocalidad terceraLocalidad=terceraLocalidadDao.findOne(terceraLocalidadId);
		if(terceraLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		
		terceraLocalidad.setNombre(terceraLocalidadDetails.getNombre());
		terceraLocalidad.setSlug(terceraLocalidadDetails.getSlug());
		terceraLocalidad.setDescripcion(terceraLocalidadDetails.getDescripcion());
		
		TerceraLocalidad updateTerceraLocalidad=terceraLocalidadDao.save(terceraLocalidad);
		return ResponseEntity.ok().body(updateTerceraLocalidad);
	
	}
	
	/*Delete an TerceraLocalidad*/
	@DeleteMapping("/terceralocalidad/{id}")
	public ResponseEntity<TerceraLocalidad> deleteTerceraLocalidad(@PathVariable(value="id") Integer terceraLocalidadId){
		
		TerceraLocalidad terceraLocalidad=terceraLocalidadDao.findOne(terceraLocalidadId);
		if(terceraLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		terceraLocalidadDao.delete(terceraLocalidad);
		
		return ResponseEntity.ok().build();
	
	}
	
}
