package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.CiudadDao;
import com.fospuca.dao.MunicipioDao;
import com.fospuca.model.Ciudad;
import com.fospuca.model.Municipio;
import com.fospuca.model.Pais;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class MunicipioController {
	@Autowired
	MunicipioDao municipioDao;
	
	/* to save an Municipio*/
	@PostMapping("/municipios")
	public Municipio createMunicipio(@Valid @RequestBody Municipio municipio) {
		return municipioDao.save(municipio);
	}
	
	/*get all Municipio*/
	@GetMapping("/municipios")
	public List<Municipio> getAllMunicipio(){
		return municipioDao.findAll();
	}
	
	/*get Municipio by MunicipioId*/
	@GetMapping("/municipios/{id}")
	public ResponseEntity<Municipio> getMunicipioById(@PathVariable(value="id") Integer municipioId){
		
		Municipio municipio=municipioDao.findOne(municipioId);
		
		if(municipio==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(municipio);
	}
	

	/*update an Municipio by MunicipioId*/
	@PutMapping("/municipios/{id}")
	public ResponseEntity<Municipio> updateMunicipio(@PathVariable(value="id") Integer ciudadId,@Valid @RequestBody Municipio municipioDetails){
		
		Municipio municipio=municipioDao.findOne(ciudadId);
		if(municipio==null) {
			return ResponseEntity.notFound().build();
		}
		
		municipio.setNombre(municipioDetails.getNombre());
		municipio.setSlug(municipioDetails.getSlug());
		municipio.setDescripcion(municipioDetails.getDescripcion());
		municipio.setCodigoMunicipio(municipioDetails.getCodigoMunicipio());
		
		Municipio updateMunicipio=municipioDao.save(municipio);
		return ResponseEntity.ok().body(updateMunicipio);
	
	}
	
	/*Delete an Municipio*/
	@DeleteMapping("/municipios/{id}")
	public ResponseEntity<Municipio> deleteMunicipio(@PathVariable(value="id") Integer municipioId){
		
		Municipio municipio=municipioDao.findOne(municipioId);
		if(municipio==null) {
			return ResponseEntity.notFound().build();
		}
		municipioDao.delete(municipio);
		
		return ResponseEntity.ok().build();
	
	}
}
