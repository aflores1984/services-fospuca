package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.EstadoDao;
import com.fospuca.model.Estado;



@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class EstadoController {
	@Autowired
	EstadoDao estadoDao;
	
	/* to save an Estado*/
	@PostMapping("/estados")
	public Estado createEstado(@Valid @RequestBody Estado estado) {
		return estadoDao.save(estado);
	}
	
	/*get all Estado*/
	@GetMapping("/estados")
	public List<Estado> getAllEsstado(){
		return estadoDao.findAll();
	}
	
	/*get Estado by EstadoId*/
	@GetMapping("/estados/{id}")
	public ResponseEntity<Estado> getEstadoById(@PathVariable(value="id") Integer estadoId){
		
		Estado estado=estadoDao.findOne(estadoId);
		
		if(estado==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(estado);
	}
	

	/*update an Estado by EstadoId*/
	@PutMapping("/estado/{id}")
	public ResponseEntity<Estado> updateEstado(@PathVariable(value="id") Integer estadoId,@Valid @RequestBody Estado estadoDetails){
		
		Estado estado=estadoDao.findOne(estadoId);
		if(estado==null) {
			return ResponseEntity.notFound().build();
		}
		
		estado.setNombre(estadoDetails.getNombre());
		estado.setSlug(estadoDetails.getSlug());
		estado.setDescripcion(estadoDetails.getDescripcion());
		estado.setCodigoEstado(estadoDetails.getCodigoEstado());
		
		Estado updateEstado=estadoDao.save(estado);
		return ResponseEntity.ok().body(updateEstado);
	
	}
	
	/*Delete an Estado*/
	@DeleteMapping("/estado/{id}")
	public ResponseEntity<Estado> deleteEstado(@PathVariable(value="id") Integer estadoId){
		
		Estado estado=estadoDao.findOne(estadoId);
		if(estado==null) {
			return ResponseEntity.notFound().build();
		}
		estadoDao.delete(estado);
		
		return ResponseEntity.ok().build();
	
	}
}
