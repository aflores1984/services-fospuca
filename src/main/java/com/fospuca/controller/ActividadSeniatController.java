package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fospuca.dao.ActividadSeniatDao;
import com.fospuca.model.ActividadSeniat;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class ActividadSeniatController {
	
	@Autowired
	ActividadSeniatDao actividadSeniatDao;
	
	/* to save an ActividadSeniat*/
	@PostMapping("/actividadseniat")
	public ActividadSeniat createActividadSeniat(@Valid @RequestBody ActividadSeniat actividadSeniat) {
		return actividadSeniatDao.save(actividadSeniat);
	}
	
	/*get all ActividadSeniat*/
	@GetMapping("/actividadseniat")
	public List<ActividadSeniat> getAllActividadSeniat(){
		return actividadSeniatDao.findAll();
	}
	
	/*get ActividadSeniat by ActividadSeniatId*/
	@GetMapping("/actividadseniat/{id}")
	public ResponseEntity<ActividadSeniat> getActividadSeniatById(@PathVariable(value="id") Integer actividadSeniatId){
		
		ActividadSeniat actividadSeniat=actividadSeniatDao.findOne(actividadSeniatId);
		
		if(actividadSeniat==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(actividadSeniat);
	}
	

	/*update an ActividadSeniat by ActividadSeniatId*/
	@PutMapping("/actividadseniat/{id}")
	public ResponseEntity<ActividadSeniat> updateActividadSeniat(@PathVariable(value="id") Integer actividadSeniatId,@Valid @RequestBody ActividadSeniat actividadSeniatDetails){
		
		ActividadSeniat actividadSeniat=actividadSeniatDao.findOne(actividadSeniatId);
		if(actividadSeniat==null) {
			return ResponseEntity.notFound().build();
		}
		
		actividadSeniat.setActividadSeniat(actividadSeniatDetails.getActividadSeniat());
		actividadSeniat.setSlug(actividadSeniatDetails.getSlug());
		actividadSeniat.setDescripcionSeniat(actividadSeniatDetails.getDescripcionSeniat());
		
		ActividadSeniat updateActividadSeniat=actividadSeniatDao.save(actividadSeniat);
		return ResponseEntity.ok().body(updateActividadSeniat);
	
	}
	
	/*Delete an ActividadSeniat*/
	@DeleteMapping("/actividadseniat/{id}")
	public ResponseEntity<ActividadSeniat> deleteActividadSeniat(@PathVariable(value="id") Integer actividadSeniatId){
		
		ActividadSeniat actividadSeniat=actividadSeniatDao.findOne(actividadSeniatId);
		if(actividadSeniat==null) {
			return ResponseEntity.notFound().build();
		}
		actividadSeniatDao.delete(actividadSeniat);
		
		return ResponseEntity.ok().build();
	
	}
	
}
