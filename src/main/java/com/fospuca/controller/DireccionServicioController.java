package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fospuca.dao.DireccionServicioDao;
import com.fospuca.model.DireccionServicio;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class DireccionServicioController {
	
	@Autowired
	DireccionServicioDao direccionServicioDao;
	
	/* to save an DireccionServicio*/
	@PostMapping("/direccionservicio")
	public DireccionServicio createDireccionFiscal(@Valid @RequestBody DireccionServicio direccionServicio) {
		return direccionServicioDao.save(direccionServicio);
	}
	
	/*get all DireccionServicio*/
	@GetMapping("/direccionservicio")
	public List<DireccionServicio> getAllDireccionServicio(){
		return direccionServicioDao.findAll();
	}
	
	/*get DireccionServicio by DireccionServicioId*/
	@GetMapping("/direccionservicio/{id}")
	public ResponseEntity<DireccionServicio> getDireccionFiscalById(@PathVariable(value="id") Integer direccionServicioId){
		
		DireccionServicio direccionServicio=direccionServicioDao.findOne(direccionServicioId);
		
		if(direccionServicio==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(direccionServicio);
	}
	

	/*update an DireccionServicio by DireccionServicioId*/
	@PutMapping("/direccionservicio/{id}")
	public ResponseEntity<DireccionServicio> updateDireccionServicio(@PathVariable(value="id") Integer direccionServicioId,@Valid @RequestBody DireccionServicio direccionServicioDetails){
		
		DireccionServicio direccionServicio=direccionServicioDao.findOne(direccionServicioId);
		if(direccionServicio==null) {
			return ResponseEntity.notFound().build();
		}
		
		direccionServicio.setNombre(direccionServicioDetails.getNombre());
		direccionServicio.setRif(direccionServicioDetails.getRif());
		direccionServicio.setObservacion(direccionServicioDetails.getObservacion());
		direccionServicio.setPosicion1(direccionServicioDetails.getPosicion1());
		direccionServicio.setPosicion2(direccionServicioDetails.getPosicion2());
		direccionServicio.setPosicion3(direccionServicioDetails.getPosicion3());
		direccionServicio.setPosicion4(direccionServicioDetails.getPosicion4());
		direccionServicio.setLocalidadCompleta(direccionServicioDetails.getLocalidadCompleta());
		direccionServicio.setFechaActivacion(direccionServicioDetails.getFechaActivacion());
		
		DireccionServicio updateDireccionServicio=direccionServicioDao.save(direccionServicio);
		return ResponseEntity.ok().body(updateDireccionServicio);
	
	}
	
	/*Delete an DireccionServicio*/
	@DeleteMapping("/direccionservicio/{id}")
	public ResponseEntity<DireccionServicio> deleteDireccionServicio(@PathVariable(value="id") Integer direccionServicioId){
		
		DireccionServicio direccionServicio=direccionServicioDao.findOne(direccionServicioId);
		if(direccionServicio==null) {
			return ResponseEntity.notFound().build();
		}
		direccionServicioDao.delete(direccionServicio);
		
		return ResponseEntity.ok().build();
	}
}
