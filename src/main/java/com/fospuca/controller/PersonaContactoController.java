package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.PersonaContactoDao;
import com.fospuca.model.PersonaContacto;


@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class PersonaContactoController {
	
	@Autowired
	PersonaContactoDao personaContactoDao;
	
	/* to save an PersonaContacto*/
	@PostMapping("/personacontacto")
	public PersonaContacto createPersonaContacto(@Valid @RequestBody PersonaContacto personaContacto) {
		return personaContactoDao.save(personaContacto);
	}
	
	/*get all PersonaContacto*/
	@GetMapping("/personacontacto")
	public List<PersonaContacto> getAllPersonaContacto(){
		return personaContactoDao.findAll();
	}
	
	/*get PersonaContacto by PersonaContactoId*/
	@GetMapping("/personacontacto/{id}")
	public ResponseEntity<PersonaContacto> getPersonaContactoById(@PathVariable(value="id") Integer personaContactoId){
		
		PersonaContacto personaContacto=personaContactoDao.findOne(personaContactoId);
		
		if(personaContacto==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(personaContacto);
	}
	

	/*update an PersonaContacto by PersonaContactoId*/
	@PutMapping("/personacontacto/{id}")
	public ResponseEntity<PersonaContacto> updatePersonaContacto(@PathVariable(value="id") Integer personaContactoId,@Valid @RequestBody PersonaContacto personaContactoDetails){
		
		PersonaContacto personaContacto=personaContactoDao.findOne(personaContactoId);
		if(personaContacto==null) {
			return ResponseEntity.notFound().build();
		}
		
		personaContacto.setCedula(personaContactoDetails.getCedula());
		personaContacto.setNombre(personaContactoDetails.getNombre());
		personaContacto.setTelefonoContacto1(personaContactoDetails.getTelefonoContacto1());
		personaContacto.setTelefonoContacto2(personaContactoDetails.getTelefonoContacto2());
		personaContacto.setTelefonoContacto3(personaContactoDetails.getTelefonoContacto3());
		personaContacto.setEmailContacto1(personaContactoDetails.getEmailContacto1());
		personaContacto.setEmailContacto2(personaContactoDetails.getEmailContacto2());
		personaContacto.setEmailContacto3(personaContactoDetails.getEmailContacto3());
		
		PersonaContacto updatePersonaContacto=personaContactoDao.save(personaContacto);
		return ResponseEntity.ok().body(updatePersonaContacto);
	
	}
	
	/*Delete an PersonaContacto*/
	@DeleteMapping("/personacontacto/{id}")
	public ResponseEntity<PersonaContacto> deletePersonaContacto(@PathVariable(value="id") Integer personaContactoId){
		
		PersonaContacto personaContacto=personaContactoDao.findOne(personaContactoId);
		if(personaContacto==null) {
			return ResponseEntity.notFound().build();
		}
		personaContactoDao.delete(personaContacto);
		
		return ResponseEntity.ok().build();
	
	}
	
}
