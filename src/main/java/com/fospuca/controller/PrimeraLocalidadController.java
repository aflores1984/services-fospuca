package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.PrimeraLocalidadDao;
import com.fospuca.model.PrimeraLocalidad;


@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class PrimeraLocalidadController {
	
	@Autowired
	PrimeraLocalidadDao primeraLocalidadDao;
	
	/* to save an PrimeraLocalidad*/
	@PostMapping("/primeralocalidad")
	public PrimeraLocalidad createPrimeraLocalidad(@Valid @RequestBody PrimeraLocalidad primeraLocalidad) {
		return primeraLocalidadDao.save(primeraLocalidad);
	}
	
	/*get all PrimeraLocalidad*/
	@GetMapping("/primeralocalidad")
	public List<PrimeraLocalidad> getAllPrimeraLocalidad(){
		return primeraLocalidadDao.findAll();
	}
	
	/*get PrimeraLocalidad by PrimeraLocalidadId*/
	@GetMapping("/primeralocalidad/{id}")
	public ResponseEntity<PrimeraLocalidad> getPrimeraLocalidadById(@PathVariable(value="id") Integer primeraLocalidadId){
		
		PrimeraLocalidad primeraLocalidad=primeraLocalidadDao.findOne(primeraLocalidadId);
		
		if(primeraLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(primeraLocalidad);
	}
	

	/*update an PrimeraLocalidad by PrimeraLocalidadId*/
	@PutMapping("/primeralocalidad/{id}")
	public ResponseEntity<PrimeraLocalidad> updatePrimeraLocalidad(@PathVariable(value="id") Integer primeralocalidadId,@Valid @RequestBody PrimeraLocalidad primeraLocalidadDetails){
		
		PrimeraLocalidad primeraLocalidad=primeraLocalidadDao.findOne(primeralocalidadId);
		if(primeraLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		
		primeraLocalidad.setNombre(primeraLocalidadDetails.getNombre());
		primeraLocalidad.setSlug(primeraLocalidadDetails.getSlug());
		primeraLocalidad.setDescripcion(primeraLocalidadDetails.getDescripcion());
		
		PrimeraLocalidad updatePrimeraLocalidad=primeraLocalidadDao.save(primeraLocalidad);
		return ResponseEntity.ok().body(updatePrimeraLocalidad);
	
	}
	
	/*Delete an PrimeraLocalidad*/
	@DeleteMapping("/primeralocalidad/{id}")
	public ResponseEntity<PrimeraLocalidad> deletePrimeraLocalidad(@PathVariable(value="id") Integer primeralocalidadId){
		
		PrimeraLocalidad primeraLocalidad=primeraLocalidadDao.findOne(primeralocalidadId);
		if(primeraLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		primeraLocalidadDao.delete(primeraLocalidad);
		
		return ResponseEntity.ok().build();
	
	}
	
}
