package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.ActividadCnaeDao;
import com.fospuca.model.ActividadCnae;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class ActividadCnaeController {
	
	@Autowired
	ActividadCnaeDao actividadCnaeDao;
	
	/* to save an ActividadCnae*/
	@PostMapping("/actividadcnae")
	public ActividadCnae createActividadCnae(@Valid @RequestBody ActividadCnae actividadCnae) {
		return actividadCnaeDao.save(actividadCnae);
	}
	
	/*get all ActividadCnae*/
	@GetMapping("/actividadcnae")
	public List<ActividadCnae> getAllActividadCnae(){
		return actividadCnaeDao.findAll();
	}
	
	/*get ActividadCnae by ActividadCnaeId*/
	@GetMapping("/actividadcnae/{id}")
	public ResponseEntity<ActividadCnae> getActividadCnaeById(@PathVariable(value="id") Integer actividadcnaeId){
		
		ActividadCnae actividadcnae=actividadCnaeDao.findOne(actividadcnaeId);
		
		if(actividadcnae==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(actividadcnae);
	}
	

	/*update an ActividadCnae by ActividadCnaeId*/
	@PutMapping("/actividadcnae/{id}")
	public ResponseEntity<ActividadCnae> updateActividadCnae(@PathVariable(value="id") Integer actividadcnaeId,@Valid @RequestBody ActividadCnae actividadcnaeDetails){
		
		ActividadCnae actividadcnae=actividadCnaeDao.findOne(actividadcnaeId);
		if(actividadcnae==null) {
			return ResponseEntity.notFound().build();
		}
		
		actividadcnae.setActividadCnae(actividadcnaeDetails.getActividadCnae());
		actividadcnae.setCodigoCnae(actividadcnaeDetails.getCodigoCnae());
		actividadcnae.setDescripcionCnae(actividadcnaeDetails.getDescripcionCnae());
		
		ActividadCnae updateActividadCnae=actividadCnaeDao.save(actividadcnae);
		return ResponseEntity.ok().body(updateActividadCnae);
	
	}
	
	/*Delete an ActividadCnae*/
	@DeleteMapping("/actividadcnae/{id}")
	public ResponseEntity<ActividadCnae> deleteActividadCnae(@PathVariable(value="id") Integer actividadCnaeId){
		
		ActividadCnae actividadCnae=actividadCnaeDao.findOne(actividadCnaeId);
		if(actividadCnae==null) {
			return ResponseEntity.notFound().build();
		}
		actividadCnaeDao.delete(actividadCnae);
		
		return ResponseEntity.ok().build();
	}
	
}
