package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fospuca.dao.DireccionFiscalDao;
import com.fospuca.model.DireccionFiscal;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class DireccionFiscalController {
	
	@Autowired
	DireccionFiscalDao direccionFiscalDao;
	
	/* to save an CuartaLocalidad*/
	@PostMapping("/direccionfiscal")
	public DireccionFiscal createDireccionFiscal(@Valid @RequestBody DireccionFiscal direccionFiscal) {
		return direccionFiscalDao.save(direccionFiscal);
	}
	
	/*get all DireccionFiscal*/
	@GetMapping("/direccionfiscal")
	public List<DireccionFiscal> getAllDireccionFiscal(){
		return direccionFiscalDao.findAll();
	}
	
	/*get DireccionFiscal by DireccionFiscalId*/
	@GetMapping("/direccionfiscal/{id}")
	public ResponseEntity<DireccionFiscal> getDireccionFiscalById(@PathVariable(value="id") Integer direccionFiscalId){
		
		DireccionFiscal direccionFiscal=direccionFiscalDao.findOne(direccionFiscalId);
		
		if(direccionFiscal==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(direccionFiscal);
	}
	

	/*update an DireccionFiscal by DireccionFiscalId*/
	@PutMapping("/direccionfiscal/{id}")
	public ResponseEntity<DireccionFiscal> updateDireccionFiscal(@PathVariable(value="id") Integer direccionFiscalId,@Valid @RequestBody DireccionFiscal direccionFiscalDetails){
		
		DireccionFiscal direccionFiscal=direccionFiscalDao.findOne(direccionFiscalId);
		if(direccionFiscal==null) {
			return ResponseEntity.notFound().build();
		}
		
		direccionFiscal.setIdentificacion(direccionFiscalDetails.getIdentificacion());
		direccionFiscal.setNombre(direccionFiscalDetails.getNombre());
		direccionFiscal.setApellido(direccionFiscalDetails.getApellido());
		direccionFiscal.setFechaActivacion(direccionFiscal.getFechaActivacion());
		direccionFiscal.setObservacion(direccionFiscalDetails.getObservacion());
		direccionFiscal.setPosicion1(direccionFiscalDetails.getPosicion1());
		direccionFiscal.setPosicion2(direccionFiscalDetails.getPosicion2());
		direccionFiscal.setPosicion3(direccionFiscalDetails.getPosicion3());
		direccionFiscal.setPosicion4(direccionFiscalDetails.getPosicion4());
		direccionFiscal.setLocalidadCompleta(direccionFiscalDetails.getLocalidadCompleta());
		
		DireccionFiscal updateDireccionFiscal=direccionFiscalDao.save(direccionFiscal);
		return ResponseEntity.ok().body(updateDireccionFiscal);
	
	}
	
	/*Delete an DireccionFiscal*/
	@DeleteMapping("/direccionfiscal/{id}")
	public ResponseEntity<DireccionFiscal> deleteDireccionFiscal(@PathVariable(value="id") Integer direccionFiscalId){
		
		DireccionFiscal direccionFiscal=direccionFiscalDao.findOne(direccionFiscalId);
		if(direccionFiscal==null) {
			return ResponseEntity.notFound().build();
		}
		direccionFiscalDao.delete(direccionFiscal);
		
		return ResponseEntity.ok().build();
	}
}
