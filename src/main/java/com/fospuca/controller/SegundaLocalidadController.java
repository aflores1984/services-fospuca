package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.SegundaLocalidadDao;
import com.fospuca.model.SegundaLocalidad;



@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class SegundaLocalidadController {
	
	@Autowired
	SegundaLocalidadDao segundaLocalidadDao;
	
	/* to save an SegundaLocalidad*/
	@PostMapping("/segundalocalidad")
	public SegundaLocalidad createSegundaLocalidad(@Valid @RequestBody SegundaLocalidad segundaLocalidad) {
		return segundaLocalidadDao.save(segundaLocalidad);
	}
	
	/*get all SegundaLocalidad*/
	@GetMapping("/segundalocalidad")
	public List<SegundaLocalidad> getAllSegundaLocalidad(){
		return segundaLocalidadDao.findAll();
	}
	
	/*get SegundaLocalidad by SegundaLocalidadId*/
	@GetMapping("/segundalocalidad/{id}")
	public ResponseEntity<SegundaLocalidad> getSegundaLocalidadById(@PathVariable(value="id") Integer segundaLocalidadId){
		
		SegundaLocalidad segundaLocalidad=segundaLocalidadDao.findOne(segundaLocalidadId);
		
		if(segundaLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(segundaLocalidad);
	}
	

	/*update an SegundaLocalidad by SegundaLocalidadId*/
	@PutMapping("/segundalocalidad/{id}")
	public ResponseEntity<SegundaLocalidad> updateSegundaLocalidad(@PathVariable(value="id") Integer segundaLocalidadId,@Valid @RequestBody SegundaLocalidad segundaLocalidadDetails){
		
		SegundaLocalidad segundaLocalidad=segundaLocalidadDao.findOne(segundaLocalidadId);
		if(segundaLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		
		segundaLocalidad.setNombre(segundaLocalidadDetails.getNombre());
		segundaLocalidad.setSlug(segundaLocalidadDetails.getSlug());
		segundaLocalidad.setDescripcion(segundaLocalidadDetails.getDescripcion());
		
		SegundaLocalidad updateSegundaLocalidad=segundaLocalidadDao.save(segundaLocalidad);
		return ResponseEntity.ok().body(updateSegundaLocalidad);
	
	}
	
	/*Delete an SegundaLocalidad*/
	@DeleteMapping("/segundalocalidad/{id}")
	public ResponseEntity<SegundaLocalidad> deleteSegundaLocalidad(@PathVariable(value="id") Integer segundaLocalidadId){
		
		SegundaLocalidad segundaLocalidad=segundaLocalidadDao.findOne(segundaLocalidadId);
		if(segundaLocalidad==null) {
			return ResponseEntity.notFound().build();
		}
		segundaLocalidadDao.delete(segundaLocalidad);
		
		return ResponseEntity.ok().build();
	
	}
	
}
