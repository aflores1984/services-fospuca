package com.fospuca.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fospuca.dao.PaisDao;
import com.fospuca.model.Pais;


@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/fospucaservices")
public class PaisController {
  @Autowired
  PaisDao paisDao;
  
  /* to save an Pais*/
	@PostMapping("/paises")
	public Pais createPais(@Valid @RequestBody Pais pais) {
		return paisDao.save(pais);
	}
	
	/*get all Pais*/
	@GetMapping("/paises")
	public List<Pais> getAllPais(){
		return paisDao.findAll();
	}
	
	/*get Pais by paisid*/
	@GetMapping("/paises/{id}")
	public ResponseEntity<Pais> getPaisById(@PathVariable(value="id") Integer paisId){
		
		Pais pais=paisDao.findOne(paisId);
		
		if(pais==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(pais);
		
	}
	
	
	/*update an Pais by paisId*/
	@PutMapping("/paises/{id}")
	public ResponseEntity<Pais> updatePais(@PathVariable(value="id") Integer paisId,@Valid @RequestBody Pais paisDetails){
		
		Pais pais=paisDao.findOne(paisId);
		if(pais==null) {
			return ResponseEntity.notFound().build();
		}
		
		pais.setNombre(paisDetails.getNombre());
		pais.setDescripcion(paisDetails.getDescripcion());
		pais.setCodigoPais(paisDetails.getCodigoPais());
		pais.setSlug(paisDetails.getSlug());
		
		Pais updatePais=paisDao.save(pais);
		return ResponseEntity.ok().body(updatePais);
		
		
		
	}
	
	/*Delete an Pais*/
	@DeleteMapping("/paises/{id}")
	public ResponseEntity<Pais> deletePais(@PathVariable(value="id") Integer paisId){
		
		Pais pais=paisDao.findOne(paisId);
		if(pais==null) {
			return ResponseEntity.notFound().build();
		}
		paisDao.delete(pais);
		
		return ResponseEntity.ok().build();
		
		
	}
}
