package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.ActividadCnae;
import com.fospuca.repository.ActividadCnaeRepository;

@Service
public class ActividadCnaeDao {
	@Autowired
	ActividadCnaeRepository actividadCnaeRepository;

	/* To save ActividadCnaeRepository */

	public ActividadCnae save(ActividadCnae actividadCnae) {
		return actividadCnaeRepository.save(actividadCnae);
	}

	/* search all ActividadCnae */

	public List<ActividadCnae> findAll() {
		return actividadCnaeRepository.findAll();
	}

	/* Get ActividadCnae */

	public ActividadCnae findOne(Integer actividadCnaeId) {
		return actividadCnaeRepository.findOne(actividadCnaeId);
	}

	/* delete an ActividadCnae */

	public void delete(ActividadCnae actividadCnae) {
		actividadCnaeRepository.delete(actividadCnae);
	}
	/**/
}
