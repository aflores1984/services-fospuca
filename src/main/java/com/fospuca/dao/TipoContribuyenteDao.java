package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.TipoContribuyente;
import com.fospuca.repository.TipoContribuyenteRepository;

@Service
public class TipoContribuyenteDao {
	@Autowired
	TipoContribuyenteRepository tipoContribuyenteRepository;

	/* To save TipoContribuyente */

	public TipoContribuyente save(TipoContribuyente tipoContribuyente) {
		return tipoContribuyenteRepository.save(tipoContribuyente);
	}

	/* search all TipoContribuyente */

	public List<TipoContribuyente> findAll() {
		return tipoContribuyenteRepository.findAll();
	}

	/* Get Id TipoContribuyente */

	public TipoContribuyente findOne(Integer tipoContribuyenteId) {
		return tipoContribuyenteRepository.findOne(tipoContribuyenteId);
	}

	/* delete an TipoContribuyente */

	public void delete(TipoContribuyente tipoContribuyente) {
		tipoContribuyenteRepository.delete(tipoContribuyente);
	}
	/**/
}
