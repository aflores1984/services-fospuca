package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.DireccionFiscal;
import com.fospuca.repository.DireccionFiscalRepository;

@Service
public class DireccionFiscalDao {
	@Autowired
	DireccionFiscalRepository direccionFiscalRepository;

	/* To save DireccionFiscal */

	public DireccionFiscal save(DireccionFiscal direccionFiscal) {
		return direccionFiscalRepository.save(direccionFiscal);
	}

	/* search all DireccionFiscal */

	public List<DireccionFiscal> findAll() {
		return direccionFiscalRepository.findAll();
	}

	/* Get DireccionFiscal */

	public DireccionFiscal findOne(Integer direccionFiscalId) {
		return direccionFiscalRepository.findOne(direccionFiscalId);
	}

	/* delete an DireccionFiscal */

	public void delete(DireccionFiscal direccionFiscal) {
		direccionFiscalRepository.delete(direccionFiscal);
	}
	/**/
}
