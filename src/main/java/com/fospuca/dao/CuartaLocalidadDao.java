package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.CuartaLocalidad;
import com.fospuca.repository.CuartaLocalidadRepository;

@Service
public class CuartaLocalidadDao {
	@Autowired
	CuartaLocalidadRepository cuartaLocalidadRepository;

	/* To save CuartaLocalidad */

	public CuartaLocalidad save(CuartaLocalidad cuartaLocalidad) {
		return cuartaLocalidadRepository.save(cuartaLocalidad);
	}

	/* search all CuartaLocalidad */

	public List<CuartaLocalidad> findAll() {
		return cuartaLocalidadRepository.findAll();
	}

	/* Get CuartaLocalidad */

	public CuartaLocalidad findOne(Integer cuartaLocalidadId) {
		return cuartaLocalidadRepository.findOne(cuartaLocalidadId);
	}

	/* delete an CuartaLocalidad */

	public void delete(CuartaLocalidad cuartaLocalidad) {
		cuartaLocalidadRepository.delete(cuartaLocalidad);
	}
	/**/
}
