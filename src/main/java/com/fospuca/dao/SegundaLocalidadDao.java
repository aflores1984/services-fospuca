package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.SegundaLocalidad;
import com.fospuca.repository.SegundaLocalidadRepository;

@Service
public class SegundaLocalidadDao {
	@Autowired
	SegundaLocalidadRepository segundaLocalidadRepository;

	/* To save SegundaLocalidad */

	public SegundaLocalidad save(SegundaLocalidad segundaLocalidad) {
		return segundaLocalidadRepository.save(segundaLocalidad);
	}

	/* search all SegundaLocalidad */

	public List<SegundaLocalidad> findAll() {
		return segundaLocalidadRepository.findAll();
	}

	/* Get Id SegundaLocalidad */

	public SegundaLocalidad findOne(Integer segundaLocalidadId) {
		return segundaLocalidadRepository.findOne(segundaLocalidadId);
	}

	/* delete an SegundaLocalidad */

	public void delete(SegundaLocalidad segundaLocalidad) {
		segundaLocalidadRepository.delete(segundaLocalidad);
	}
	/**/
}
