package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.Pais;
import com.fospuca.repository.PaisRepository;


@Service
public class PaisDao {
	
	@Autowired
	PaisRepository paisRepository;

	/* To save Pais */

	public Pais save(Pais pais) {
		return paisRepository.save(pais);
	}

	/* search all Pais */

	public List<Pais> findAll() {
		return paisRepository.findAll();
	}

	/* Get Pais */

	public Pais findOne(Integer paisid) {
		return paisRepository.findOne(paisid);
	}

	/* delete an Pais */

	public void delete(Pais pais) {
		paisRepository.delete(pais);
	}
	/**/
}
