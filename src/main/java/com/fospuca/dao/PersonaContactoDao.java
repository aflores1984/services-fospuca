package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.PersonaContacto;
import com.fospuca.repository.PersonaContactoRepository;


@Service
public class PersonaContactoDao {
	
	@Autowired
	PersonaContactoRepository personaContactoRepository;

	/* To save PersonaContactoRepository */

	public PersonaContacto save(PersonaContacto personaContacto) {
		return personaContactoRepository.save(personaContacto);
	}

	/* search all PersonaContacto */

	public List<PersonaContacto> findAll() {
		return personaContactoRepository.findAll();
	}

	/* Get PersonaContacto */

	public PersonaContacto findOne(Integer personaContactoId) {
		return personaContactoRepository.findOne(personaContactoId);
	}

	/* delete an PersonaContacto */

	public void delete(PersonaContacto personaContacto) {
		personaContactoRepository.delete(personaContacto);
	}
	/**/
}
