package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.PrimeraLocalidad;
import com.fospuca.repository.PrimeraLocalidadRepository;

@Service
public class PrimeraLocalidadDao {
	@Autowired
	PrimeraLocalidadRepository primeraLocalidadRepository;

	/* To save PrimeraLocalidad */

	public PrimeraLocalidad save(PrimeraLocalidad primeraLocalidad) {
		return primeraLocalidadRepository.save(primeraLocalidad);
	}

	/* search all PrimeraLocalidad */

	public List<PrimeraLocalidad> findAll() {
		return primeraLocalidadRepository.findAll();
	}

	/* Get PrimeraLocalidad */

	public PrimeraLocalidad findOne(Integer primeraLocalidadId) {
		return primeraLocalidadRepository.findOne(primeraLocalidadId);
	}

	/* delete an PrimeraLocalidad */

	public void delete(PrimeraLocalidad primeraLocalidad) {
		primeraLocalidadRepository.delete(primeraLocalidad);
	}
	/**/
}
