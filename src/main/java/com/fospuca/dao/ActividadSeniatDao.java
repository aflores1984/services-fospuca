package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.ActividadSeniat;
import com.fospuca.repository.ActividadSeniatRepository;

@Service
public class ActividadSeniatDao {
	@Autowired
	ActividadSeniatRepository actividadSeniatRepository;

	/* To save ActividadSeniatRepository */

	public ActividadSeniat save(ActividadSeniat actividadSeniat) {
		return actividadSeniatRepository.save(actividadSeniat);
	}

	/* search all ActividadSeniat */

	public List<ActividadSeniat> findAll() {
		return actividadSeniatRepository.findAll();
	}

	/* Get ActividadSeniat */

	public ActividadSeniat findOne(Integer actividadSeniatId) {
		return actividadSeniatRepository.findOne(actividadSeniatId);
	}

	/* delete an ActividadSeniat */

	public void delete(ActividadSeniat actividadSeniat) {
		actividadSeniatRepository.delete(actividadSeniat);
	}
	/**/
}
