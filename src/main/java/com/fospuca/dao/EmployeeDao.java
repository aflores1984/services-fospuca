package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.Employee;
import com.fospuca.repository.EmployeeRepository;

@Service
public class EmployeeDao {
	@Autowired
	EmployeeRepository employeeRepository;

	/* To save Employee */

	public Employee save(Employee employee) {
		return employeeRepository.save(employee);
	}

	/* search all Employees */

	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	/* Get Employee */

	public Employee findOne(Long empid) {
		return employeeRepository.findOne(empid);
	}

	/* delete an Employee */

	public void delete(Employee employee) {
		employeeRepository.delete(employee);
	}
	/**/
}
