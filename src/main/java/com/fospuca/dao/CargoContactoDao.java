package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.CargoContacto;
import com.fospuca.repository.CargoContactoRepository;

@Service
public class CargoContactoDao {
	@Autowired
	CargoContactoRepository cargoContactoRepository;

	/* To save CargoContacto */

	public CargoContacto save(CargoContacto cargoContacto) {
		return cargoContactoRepository.save(cargoContacto);
	}

	/* search all CargoContacto */

	public List<CargoContacto> findAll() {
		return cargoContactoRepository.findAll();
	}

	/* Get CargoContacto */

	public CargoContacto findOne(Integer cargoContactoId) {
		return cargoContactoRepository.findOne(cargoContactoId);
	}

	/* delete an CargoContacto */

	public void delete(CargoContacto cargoContacto) {
		cargoContactoRepository.delete(cargoContacto);
	}
	/**/
}
