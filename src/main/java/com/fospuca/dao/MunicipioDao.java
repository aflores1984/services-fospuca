package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.Municipio;
import com.fospuca.repository.MunicipioRepository;

@Service
public class MunicipioDao {
	@Autowired
	MunicipioRepository municipioRepository;

	/* To save Municipio */

	public Municipio save(Municipio municipio) {
		return municipioRepository.save(municipio);
	}

	/* search all Municipio */

	public List<Municipio> findAll() {
		return municipioRepository.findAll();
	}

	/* Get Municipio */

	public Municipio findOne(Integer municipioId) {
		return municipioRepository.findOne(municipioId);
	}

	/* delete an Municipio */

	public void delete(Municipio municipio) {
		municipioRepository.delete(municipio);
	}
	/**/
}
