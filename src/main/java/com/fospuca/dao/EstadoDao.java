package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.Estado;
import com.fospuca.repository.EstadoRepository;

@Service
public class EstadoDao {
	
	@Autowired
	EstadoRepository estadoRepository;

	/* To save Estado */

	public Estado save(Estado estado) {
		return estadoRepository.save(estado);
	}

	/* search all Estado */

	public List<Estado> findAll() {
		return estadoRepository.findAll();
	}

	/* Get Estado */

	public Estado findOne(Integer estadoid) {
		return estadoRepository.findOne(estadoid);
	}

	/* delete an Estado */

	public void delete(Estado estado) {
		estadoRepository.delete(estado);
	}
	/**/
}
