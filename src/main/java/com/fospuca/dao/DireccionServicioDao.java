package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.DireccionServicio;
import com.fospuca.repository.DireccionServicioRepository;

@Service
public class DireccionServicioDao {
	
	@Autowired
	DireccionServicioRepository direccionServicioRepository;

	/* To save DireccionServicioJuridico */

	public DireccionServicio save(DireccionServicio direccionServicio) {
		return direccionServicioRepository.save(direccionServicio);
	}

	/* search all DireccionServicioJuridico */

	public List<DireccionServicio> findAll() {
		return direccionServicioRepository.findAll();
	}

	/* Get DireccionServicioJuridico */

	public DireccionServicio findOne(Integer direccionServicioId) {
		return direccionServicioRepository.findOne(direccionServicioId);
	}

	/* delete an DireccionServicioJuridico */

	public void delete(DireccionServicio direccionServicio) {
		direccionServicioRepository.delete(direccionServicio);
	}
	/**/
}
