package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.Ciudad;
import com.fospuca.repository.CiudadRepository;

@Service
public class CiudadDao {
	@Autowired
	CiudadRepository ciudadRepository;

	/* To save Ciudad */

	public Ciudad save(Ciudad ciudad) {
		return ciudadRepository.save(ciudad);
	}

	/* search all Ciudad */

	public List<Ciudad> findAll() {
		return ciudadRepository.findAll();
	}

	/* Get Ciudad */

	public Ciudad findOne(Integer ciudadId) {
		return ciudadRepository.findOne(ciudadId);
	}

	/* delete an Ciudad */

	public void delete(Ciudad ciudad) {
		ciudadRepository.delete(ciudad);
	}
	/**/
}
