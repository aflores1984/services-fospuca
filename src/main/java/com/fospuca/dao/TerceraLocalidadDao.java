package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.TerceraLocalidad;
import com.fospuca.repository.TerceraLocalidadRepository;

@Service
public class TerceraLocalidadDao {
	@Autowired
	TerceraLocalidadRepository terceraLocalidadRepository;

	/* To save TerceraLocalidad */

	public TerceraLocalidad save(TerceraLocalidad terceraLocalidad) {
		return terceraLocalidadRepository.save(terceraLocalidad);
	}

	/* search all TerceraLocalidad */

	public List<TerceraLocalidad> findAll() {
		return terceraLocalidadRepository.findAll();
	}

	/* Get Id TerceraLocalidad */

	public TerceraLocalidad findOne(Integer terceraLocalidadId) {
		return terceraLocalidadRepository.findOne(terceraLocalidadId);
	}

	/* delete an TerceraLocalidad */

	public void delete(TerceraLocalidad terceraLocalidad) {
		terceraLocalidadRepository.delete(terceraLocalidad);
	}
	/**/
}
