package com.fospuca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fospuca.model.Parroquia;
import com.fospuca.repository.ParroquiaRepository;

@Service
public class ParroquiaDao {
	
	@Autowired
	ParroquiaRepository parroquiaRepository;

	/* To save Parroquia */

	public Parroquia save(Parroquia parroquia) {
		return parroquiaRepository.save(parroquia);
	}

	/* search all Parroquia */

	public List<Parroquia> findAll() {
		return parroquiaRepository.findAll();
	}

	/* Get Parroquia */

	public Parroquia findOne(Integer parroquiaid) {
		return parroquiaRepository.findOne(parroquiaid);
	}

	/* delete an Parroquia */

	public void delete(Parroquia parroquia) {
		parroquiaRepository.delete(parroquia);
	}
	/**/
}
